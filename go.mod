module code.carvers.co/subtitler

require (
	bou.ke/monkey v1.0.1 // indirect
	github.com/fatih/color v1.7.0 // indirect
	github.com/mattn/go-colorable v0.0.9 // indirect
	github.com/mattn/go-isatty v0.0.4 // indirect
	github.com/otiai10/gosseract v2.2.1+incompatible
	github.com/otiai10/mint v1.2.1 // indirect
	github.com/remko/go-mkvparse v0.0.0-20181231110406-e9dc20e4201a
	github.com/stretchr/testify v1.3.0 // indirect
	golang.org/x/net v0.0.0-20190125091013-d26f9f9a57f3 // indirect
	golang.org/x/sys v0.0.0-20190201152629-afcc84fd7533 // indirect
	yall.in v0.0.1
)
