package rle

import (
	"encoding/binary"
	"fmt"
	"image"
	"image/color"
)

func ToNRGBA(data []byte, width, height int, palette map[byte]color.NYCbCrA) (*image.NRGBA, error) {
	offset := 0
	line := 0
	column := 0
	img := image.NewNRGBA(image.Rectangle{
		Min: image.Point{X: 0, Y: 0},
		Max: image.Point{X: width, Y: height},
	})
	for offset < len(data) && line < height {
		run := 1
		colorID := data[offset]
		// move past that first byte
		offset++
		if colorID == 0x00 {
			// if the entire first byte is 0, that indicates
			// a run, and we need to figure out how long that
			// run is

			// set the run length to the last six bits of the
			// second byte, because it will be _at least_ that
			// long.
			run = int(data[offset] & 0x3f)
			var colorAfterRun bool
			if data[offset]&0x80 != 0 {
				// if the first bit is 1, the color will
				// follow the run length. If not, the color
				// is 0.
				colorAfterRun = true
			}
			if data[offset]&0x40 != 0 {
				// if the second bit is 1, the run length is
				// over 63, meaning it stretches into the
				// next byte.
				run = int(binary.BigEndian.Uint16([]byte{data[offset] & 0x3f, data[offset+1]}))
				// move past the second byte
				offset++
			}
			// move past the second byte, or the third byte
			// if the run length stretched into 2 bytes
			offset++
			if colorAfterRun {
				colorID = data[offset]
				// if the color ran into a fourth byte, move
				// past that, too
				offset++
			}
		}
		if run == 0 {
			// new line, check to make sure we wrote all the pixels
			if column%width != 0 {
				return nil, fmt.Errorf("decoded %d pixels, each line should be %d pixels",
					column, width)
			}
			line++
			column = 0
			continue
		}
		if column+run > width {
			return nil, fmt.Errorf("trying to add %d more pixels to row, but already have %d, and width is only %d",
				run, column, width)
		}
		// get the color from the palette
		yCbCrA, ok := palette[colorID]
		if !ok {
			if colorID == 255 {
				// assume it's transparent
				yCbCrA = color.NYCbCrA{
					YCbCr: color.YCbCr{
						Y:  0,
						Cb: 0,
						Cr: 0,
					},
					A: 0,
				}
			} else {
				return nil, fmt.Errorf("no color with ID %d in palette", colorID)
			}
		}
		// set the pixels
		for i := 0; i < run; i++ {
			img.Set(column+i, line, yCbCrA)
		}
		column += run
	}
	return img, nil
}
