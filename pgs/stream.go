package pgs

import (
	"fmt"
	"image"
	"time"

	"code.carvers.co/subtitler/rle"
)

const (
	SUPSegPDS = 0x14
	SUPSegODS = 0x15
	SUPSegPCS = 0x16
	SUPSegWDS = 0x17
	SUPSegEND = 0x80
)

// Segment represents a single segment of a Stream. Generally, these are
// individual BlockElements from an MKV file, or segments from a PGS file.
// The Segments should have their header parsed out before the data is set
// in the Bytes property, setting only the amount of data in the PGS segment's
// Size field.
type Segment struct {
	PresentationTimestamp int64
	SegmentType           byte
	Bytes                 []byte
	LastInBlock           bool
}

type Subtitle struct {
	Start  time.Duration
	End    time.Duration
	Forced bool
	X      int64
	Y      int64
	Image  image.Image
}

// Stream represents a Presentation Graphic Stream, or a description of
// the graphics that should be displayed on screen over a period of time.
type Stream struct {
	DisplaySets []DisplaySet
}

// ParseStream returns a stream from a bunch of Segments.
func ParseStream(segments []Segment) (Stream, error) {
	var stream Stream
	var subPicture *DisplaySet
	var lastODS ODS
	for _, segment := range segments {
		switch segment.SegmentType {
		case SUPSegPDS:
			pds, err := decodePDS(segment.Bytes)
			if err != nil {
				return stream, fmt.Errorf("error decoding pds segment: %s", err.Error())
			}
			if subPicture == nil {
				return stream, fmt.Errorf("unexpected PDS segement without a PCS segment")
			}
			pds.LastInBlock = segment.LastInBlock
			subPicture.DefinitionSegments = append(subPicture.DefinitionSegments, DS{Palette: pds})
		case SUPSegODS:
			ods, err := decodeODS(segment.Bytes, lastODS)
			if err != nil {
				return stream, fmt.Errorf("error decoding ods segment: %s", err.Error())
			}
			if subPicture == nil {
				return stream, fmt.Errorf("unexpected ODS segement without a PCS segment")
			}
			lastODS = ods
			if subPicture.ControlSegment.PaletteUpdateFlag == 0x80 {
				continue
			}
			subPicture.DefinitionSegments = append(subPicture.DefinitionSegments, DS{Object: ods})
		case SUPSegPCS:
			// PCS denotes the beginning of a new sub picture
			pcs, err := decodePCS(segment.Bytes)
			if err != nil {
				return stream, fmt.Errorf("error decoding pcs segment: %s", err.Error())
			}
			switch pcs.CompositionState {
			case 0x80:
				subPicture = &DisplaySet{
					ControlSegment: pcs,
					Start:          segment.PresentationTimestamp,
				}
			case 0x00, 0x40, 0xc0:
				if subPicture == nil {
					return stream, fmt.Errorf("missing start of epoch")
				}
			default:
				return stream, fmt.Errorf("unknown composition state: %x", pcs.CompositionState)
			}
		case SUPSegWDS:
			num := segment.Bytes[0]
			offset := 1
			var windows []WDS
			for i := 0; i < int(num); i++ {
				wds, err := decodeWDS(segment.Bytes[offset:])
				if err != nil {
					return stream, fmt.Errorf("error decoding wds segment: %s", err.Error())
				}
				if subPicture == nil {
					return stream, fmt.Errorf("unexpected WDS segment without a PCS segment")
				}
				windows = append(windows, wds)
				offset += 9
			}
			subPicture.DefinitionSegments = append(subPicture.DefinitionSegments, DS{Windows: windows})
		case SUPSegEND:
			if subPicture == nil {
				return stream, fmt.Errorf("unexpected end segment without an epoch")
			}
			stream.DisplaySets = append(stream.DisplaySets, *subPicture)
		}
	}
	return stream, nil
}

func SubtitlesFromStream(stream Stream) ([]Subtitle, error) {
	var res []Subtitle
	for pos, ds := range stream.DisplaySets {
		objects := map[uint16]ODS{}
		palettes := map[uint16]PDS{}
		var palette PDS
		for _, segment := range ds.DefinitionSegments {
			if len(segment.Palette.Entries) > 0 {
				palette = segment.Palette
			}
			if len(segment.Object.Data) > 0 {
				if len(palette.Entries) < 1 {
					return nil, fmt.Errorf("Object before palette")
				}
				objects[segment.Object.ID] = segment.Object
				palettes[segment.Object.ID] = palette
			}
		}
		for _, compObject := range ds.ControlSegment.CompositionObjects {
			object, ok := objects[compObject.ObjectID]
			if !ok {
				return nil, fmt.Errorf("no object %d found for ds %d",
					compObject.ObjectID, pos)
			}
			palette, ok := palettes[compObject.ObjectID]
			if !ok {
				return nil, fmt.Errorf("no palette %d found for ds %d",
					compObject.ObjectID, pos)
			}
			img, err := rle.ToNRGBA(object.Data, int(object.Width), int(object.Height), palette.Entries)
			if err != nil {
				return nil, err
			}
			res = append(res, Subtitle{Image: img})
		}
	}
	return res, nil
}
