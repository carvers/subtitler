package pgs

import (
	"encoding/binary"
	"fmt"
	"image/color"
)

const (
	odsFirstInSequenceFlag = 0x80
	odsLastInSequenceFlag  = 0x40
)

type DisplaySet struct {
	Start              int64
	ControlSegment     PCS
	DefinitionSegments []DS
}

type DS struct {
	Windows []WDS
	Palette PDS
	Object  ODS
}

type WDS struct {
	ID                 uint8
	HorizontalPosition uint16
	VerticalPosition   uint16
	Width              uint16
	Height             uint16
}

func decodeWDS(b []byte) (WDS, error) {
	if len(b) < 9 {
		return WDS{}, fmt.Errorf("expected at least %d bytes in WDS segment, got %d", 9, len(b))
	}
	w := WDS{
		ID:                 b[0],
		HorizontalPosition: binary.BigEndian.Uint16(b[1:3]),
		VerticalPosition:   binary.BigEndian.Uint16(b[3:5]),
		Width:              binary.BigEndian.Uint16(b[5:7]),
		Height:             binary.BigEndian.Uint16(b[7:9]),
	}
	return w, nil
}

type PDS struct {
	ID            byte
	VersionNumber byte
	Entries       map[byte]color.NYCbCrA
	LastInBlock   bool
}

func decodePDS(b []byte) (PDS, error) {
	if len(b) < 2 {
		return PDS{}, fmt.Errorf("expected at least %d bytes in PDS segment, got %d", 2, len(b))
	}
	p := PDS{
		ID:            b[0],
		VersionNumber: b[1],
		Entries:       map[byte]color.NYCbCrA{},
	}
	numPalettes := (len(b) - 2) / 5
	if numPalettes*5+2 != len(b) {
		return PDS{}, fmt.Errorf("Ignored bytes in palette: expected %d bytes, only got %d bytes", len(b), numPalettes*5+2)
	}
	for i := 0; i < numPalettes; i++ {
		pEntry := b[i*5+2:]
		id, entry, err := decodeNYCbCrA(pEntry)
		if err != nil {
			return PDS{}, err
		}
		p.Entries[id] = entry
	}
	return p, nil
}

func decodeNYCbCrA(b []byte) (uint8, color.NYCbCrA, error) {
	if len(b) < 5 {
		return 0, color.NYCbCrA{}, fmt.Errorf("expected at least %d bytes in PDS segment, got %d", 5, len(b))
	}
	return uint8(b[0]), color.NYCbCrA{
		YCbCr: color.YCbCr{
			Y:  uint8(b[1]),
			Cr: uint8(b[2]),
			Cb: uint8(b[3]),
		},
		A: uint8(b[4]),
	}, nil
}

type ODS struct {
	ID                 uint16
	VersionNumber      byte
	LastInSequenceFlag byte
	DataLength         uint32 // only uses 3 bytes, really
	Width              uint16
	Height             uint16
	Data               []byte
}

func decodeODS(b []byte, o ODS) (ODS, error) {
	if len(b) < 5 {
		return ODS{}, fmt.Errorf("expected at least %d bytes in ODS segment, got %d", 5, len(b))
	}
	o.ID = binary.BigEndian.Uint16(b[:2])
	o.VersionNumber = b[2]
	o.LastInSequenceFlag = b[3]
	if o.LastInSequenceFlag&odsFirstInSequenceFlag != 0 {
		o.DataLength = binary.BigEndian.Uint32(append([]byte{0}, b[4:7]...))
		o.Width = binary.BigEndian.Uint16(b[7:9])
		o.Height = binary.BigEndian.Uint16(b[9:11])
		o.Data = b[11:]
	} else {
		o.Data = append(o.Data, b[4:]...)
	}
	if o.LastInSequenceFlag&odsLastInSequenceFlag != 0 && len(o.Data) != int(o.DataLength)-4 { // subtract width/height bytes from data length
		return ODS{}, fmt.Errorf("expected %d bytes in ODS segment data, got %d", o.DataLength, len(o.Data))
	}
	return o, nil
}
