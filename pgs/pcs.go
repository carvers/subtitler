package pgs

import (
	"encoding/binary"
	"fmt"
)

const (
	maxEpochPalettes = 8
	maxEpochObjects  = 64
	maxObjectRefs    = 2
)

type PCS struct {
	Width                      uint16
	Height                     uint16
	FrameRate                  byte
	CompositionNumber          uint16
	CompositionState           uint8
	PaletteUpdateFlag          uint8
	PaletteID                  uint8
	NumberOfCompositionObjects byte
	CompositionObjects         []CompositionObject
}

type CompositionObject struct {
	ObjectID                   uint16
	WindowID                   byte
	CompositionFlag            byte
	HorizontalPosition         uint16
	VerticalPosition           uint16
	CroppingHorizontalPosition uint16
	CroppingVerticalPosition   uint16
	CroppingWidth              uint16
	CroppingHeight             uint16
}

// Size returns the number of bytes the CompositionObject took up
// in the encoding.
func (c CompositionObject) Size() int {
	if c.CompositionFlag == 0x80 {
		// extra fields are set if CompositionFlag is set to crop (0x80)
		return 16
	}
	return 8
}

func decodePCS(b []byte) (PCS, error) {
	if len(b) < 11 {
		return PCS{}, fmt.Errorf("expected at least %d bytes in PCS segment, got %d", 11, len(b))
	}
	p := PCS{
		Width:                      binary.BigEndian.Uint16(b[0:2]),
		Height:                     binary.BigEndian.Uint16(b[2:4]),
		FrameRate:                  b[4],
		CompositionNumber:          binary.BigEndian.Uint16(b[5:7]),
		CompositionState:           b[7],
		PaletteUpdateFlag:          b[8],
		PaletteID:                  b[9],
		NumberOfCompositionObjects: b[10],
	}
	if p.NumberOfCompositionObjects > maxObjectRefs {
		return PCS{}, fmt.Errorf("error decoding PCS: max number of composition objects per segment is %d, got %d", maxObjectRefs, p.NumberOfCompositionObjects)
	}
	offset := 11 // start with the offset for the PCS object
	for i := 0; i < int(p.NumberOfCompositionObjects); i++ {
		compObj, err := decodeCompObject(b[offset:])
		if err != nil {
			return PCS{}, fmt.Errorf("error decoding composition object #%d/%d: %s", i, b[10], err.Error())
		}
		p.CompositionObjects = append(p.CompositionObjects, compObj)
		offset += compObj.Size()
	}
	return p, nil
}

func decodeCompObject(b []byte) (CompositionObject, error) {
	if len(b) < 8 {
		return CompositionObject{}, fmt.Errorf("not enough bytes for composition object, needed at least 8, only have %d: % x", len(b), b)
	}
	comp := CompositionObject{
		ObjectID:           binary.BigEndian.Uint16(b[:2]),
		WindowID:           b[2],
		CompositionFlag:    b[3],
		HorizontalPosition: binary.BigEndian.Uint16(b[4:6]),
		VerticalPosition:   binary.BigEndian.Uint16(b[6:8]),
	}
	if comp.CompositionFlag == 0x80 {
		comp.CroppingHorizontalPosition = binary.BigEndian.Uint16(b[8:10])
		comp.CroppingVerticalPosition = binary.BigEndian.Uint16(b[10:12])
		comp.CroppingWidth = binary.BigEndian.Uint16(b[12:14])
		comp.CroppingHeight = binary.BigEndian.Uint16(b[14:16])
	}
	return comp, nil
}
