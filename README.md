# subtitler

A Go utility for parsing [MKV][mkv] files, extracting the [PGS][pgs] subtitles
from them, performing [OCR][ocr] on the subtitles, and generating [ASS][ass]
subtitles.

## Install

* Download one of the [release archives][releases] and extract it. Move the
  binary to `/usr/local/bin`, `/usr/bin`, or anywhere else in your `$PATH`.
* Install [tesseract][tesseract] (maybe?)

## Usage

```sh $ subtitler /path/to/my/mkv/file --lang en --out /where/to/create/subtitle.ass ```

Arguments:

```
--help		display help text
--lang  	language of subtitles to extract
--debug		run in debug mode, not generating subtitles but identifying the
		structure of the embedded PGS subtitles
--parse-only	only parse the MKV file, saving the PGS file to the location
		specified in --out
--as-png	only parse the MKV file and the PGS encoding, outputing each
		subtitle as a PNG in the folder specified in out. A JSON file
		containing the timings and the image displayed at each timing
		will also be saved.
--out		where to save any files to be written.
			In normal operation, this will be a .ass file.
			For --parse-only, this will be a .pgs file.
			For --debug, this will be a .json file.
			For --as-png, this should be a directory, which will be
			filled with .png files and a .json file.
```

[mkv]: https://en.wikipedia.org/wiki/Matroska "Matroska file format on Wikipedia"
[pgs]: https://patents.google.com/patent/US20090185789/da "Patent describing PGS subtitles"
[ocr]: https://en.wikipedia.org/wiki/Optical_character_recognition "Optical character recognition on Wikipedia"
[ass]: https://www.matroska.org/technical/specs/subtitles/ssa.html "SSA/ASS subtitles on Matroska website"
[releases]: https://gitlab.com/carvers/subtitler/releases "Releases of subtitler"
[tesseract]: https://github.com/tesseract-ocr/tesseract/ "Tesseract on GitHub"
