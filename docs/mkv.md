# MKV

An MKV file has at least one [EBML document][ebml-doc], and each document must
start with an [EBML header][ebml-header], followed by an EBML root element,
which Matroska calls a **"segment"**. It's important to note that EBML is
inspired by XML, and is therefore hierarchical; for example, the segment wraps
the top-level elements, so it begins before them and ends after them. The
[parsing library][go-mkvparse] we use offers callbacks at the beginning _and_
the end of an element, leaving it up to the user to keep track of where in the
hierarchy they are.

## Segments

Inside a segment, there are multiple top-level elements that can occur:

* SeekHead
* Info
* Tracks
* Chapters
* Cluster
* Cues
* Attachments
* Tags

### SeekHead

SeekHead, also known as MetaSeek, provides an index of top level elements
locations in a segment. It's an easy and convenient way to find the elements
you're interested in. Each SeekHead element has one or more child Seek
elements, each containing a SeekID and SeekPosition.

### Info

The info element contains information on the Segment, including its ID,
filename, any linked IDs and filenames, the "family" it belongs to,
ChapterTranslate (what does that do?), a TimestampScale, a Duration, a DateUTC,
a Title, information on the app that muxed it, and information on the app that
wrote it.

### Tracks

The tracks element contains information for each track in a Segment. It
contains TrackEntry elements, each containing a track number, a unique
identifier, a type of track (audio, video, subtitles, etc.), a name for the
track, a language for the track, an ID for the codec the track contains, a
place to put data that has a specific meaning to the codec, the codec's name,
and if applicable, video and/or audio metadata.

### Chapters

The chapters element contains the chapter definitions for a Segment. It
contains EditionEntry elements, each containing a unique identifier, a hidden
flag, a default flag, and an ordered flag, along with a ChapterAtom segment,
containing the chapter unique identifier, a string version of the unique
identifier, the time the chapter starts, the time the chapter ends, a hidden
flag, and a display element, consisting of a string and a language.

### Cluster

Cluster elements contain the actual data for tracks, like video frames or (for
our purposes) PGS data. Cluster elements consist of BlockGroup and/or
SimpleBlock elements and/or EncryptedBlock elements, along with a timestamp for
when the first block should be played relative to the Segment, information on
silent tracks, a position, and a previous size. (?)

The timestamp element _must_ occur before any blocks.

#### Blocks

Blocks start with a bit flag for the data type of the block, followed by a
header consisting of the track number, a timestamp relative to the Cluster's
timestamp, and flags, followed by an optional frame size, followed by the data
frame.

### Cues

Cue elements are aids for players that provide a time index for some of the
Track elements. It's used for seeking to a specific time during playback, just
like the SeekHead element is used for seeking to a specific element. The Cues
element consists of one or more CuePoint elements, themselves consisting of a
CueTime element and a CueTrackPositions element.

### Attachments

Attachments elements are for attaching files to an MKV file like pictures,
webpages, executables, or whatever. They're composed of AttachedFile elements,
which are composed of a file description, a file name, a mime type, a data
block, a unique identifier, a referral, a used start time, and a used end time.

### Tags

Tags elements are for attaching metadata to a Segment. Each Track or Chapter
that has its unique ID listed in the Tags element has the specified tags
applied to it.

## Ordering

Within a Segment, there must be _at least_ an Info element. For a file to be
playable, there must be at least an Info element, a Tracks element, and a
Cluster element.

The first Info and Tracks elements _must_ be before the first Cluster element,
_or_ there must be a SeekHead element _before_ the first Cluster element that
references the first Info and Tracks elements.

The SeekHead element _should_ be the first element in a Segment (except for a
CRC-32 element, which is possible but not recommended), but there is no
guarantee. If there is more than one SeekHead element, the first SeekHead
element _must_ reference the identity and position of the next SeekHead
element. Also, any SeekHead elements past the first _must_ reference only
Cluster elements. All top-level elements _must_ be referenced by the SeekHead
elements.

The Info element _should_, but may not, occur before the first Tracks and
Cluster elements.

The Chapters element _should_, but may not, occur before the Cluster elements,
and before the first Tracks element but after the first Info element.

The Attachments element can be placed before or after the Cluster elements.

The Tags element _should_, but may not be, placed at the end of the Segment.

## Sources

* [Matroska Specification][matroska-spec]
* [Matroska element ordering][matroska-order]

[matroska-spec]: http://matroska-org.github.io/matroska-specification/diagram.html "Matroska Specification"
[matroska-order]: http://matroska-org.github.io/matroska-specification/ordering.html "Matroska Element Ordering"
[ebml-doc]: https://github.com/Matroska-Org/ebml-specification/blob/master/specification.markdown#ebml-document "EBML Document in the EBML spec"
[ebml-header]: https://github.com/Matroska-Org/ebml-specification/blob/master/specification.markdown#ebml-header "EBML Header in the EBML spec"
[go-mkvparse]: https://github.com/remko/go-mkvparse "go-mkvparse on GitHub"
