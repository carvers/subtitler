# PGS

PGS subtitles ("HDMV presentation graphics stream subtitles", honestly) are
images that are overlaid on a video stream to create subtitles.  It is the most
popular way to subtitle Blu-ray movies.

This document is largely a rehashing of the [excellent blog post][scorpius] by
thescorpius.com.

## Segments

PGS subtitles are made of segments in a linear layout. Each segment starts with
2 bytes for a magic number denoting the start of a segment, then 4 bytes
dedicated to the presentation timestamp, 4 bytes dedicated to the decoding
timestamp, 1 byte dedicated to the segment type, and 2 bytes dedicated to the
segment size. After that, however many bytes are indicated as the segment size
are devoted to the segment data, and that's the end of the segment.

### Magic Number

Every segment begins with a magic number that happens to translate to "PG" in
hexadecimal: `0x5047`.

### Presentation Timestamp

The presentation timestamp is the time when the sub picture should be shown on
screen. This is stored at 90 kHz, so to get milliseconds, we should divide the
decimal value of the presentation timestamp by 90, and that's the timestamp in
milliseconds.

### Decoding Timestamp

The decoding timestamp is the time when the decoding of the segment should
start. This is almost always zero, and subtitler will ignore it.

### Segment Type

There are five segment types:

* Presentation Composition Segment
* Window Definition Segment
* Palette Definition Segment
* Object Definition Segment
* End of Display Set Segment

### Presentation Composition Segment

The presentation composition segment, sometimes called the control segment, is
used to start a new display set. PGS is a format for composing pictures, and
the PCS segment documents how they should be composed.

Its first two bytes (following the header) are for the width of the video, in
pixels. The next two are for the height, also in pixels.

The next byte is the framerate. In practice, this is always `0x10`, and
subtitler will always ignore it.

The next two bytes are a number for this specific composition. This number
increments every time a graphics update occurs.

The next byte is a composition state. It can be one of four values:

* `0x00` (Normal): A display update. Only new objects different from the last
  display set will be included. Usually this is used to stop displaying objects
  on the screen, by defining a presentation composition segment with no
  objects, but it can also be used to update what's on the screen.
* `0x40` (Acquisition point): A display refresh. Only new objects will be
  included in this display set. If a new object shares an ID with an object
  already in the display set, the new object should replace the old object.
* `0x80` (Epoch start): A new display. All the segments to display a new
  composition on the screen will be in this display set.

The next byte is a palette update flag. This is used when only the palette of
the display is changing. It can be `0x00` (false) or `0x80` (true).

The next byte is an ID for the palette to be used, if this is a palette only
display update.

The final byte is the number of composition objects that's defined in this
segment.

All the remaining bytes in the presentation composition segment are used for
composition objects.

#### Composition Object

Composition objects define where on the screen an image will be shown.

The first two bytes are the ID of the object definition segment that contains
the image data.

The next byte is the ID of the window definition segment that contains the
window information for the object. Each window may only have up to two objects.

The next byte is a flag to determine whether or not to force display of a
cropped image object. `0x40` forces display, `0x00` does not.

The next two bytes are the X offset from the left edge of the screen for the
top left pixel of the image.

The next two bytes are the Y offset from the top edge of the screen for the top
left pixel of the image.

The next two bytes are the X offset from the left edge of the screen for the
top left pixel of the cropped image. This is only used when the cropped object
flag is set to 0x40.

The next two bytes are the Y offset from the top edge of the screen for the top
left pixel of the cropped image. This is only used when the cropped object flag
is set to 0x40.

The next two bytes are the width in pixels of the cropped object. This is only
used when the cropped object flag is set to 0x40.

The next two bytes are the height in pixels of the cropped object. This is only
used when the cropped object flag is set to 0x40.

Generally, the cropped object bytes are only used to show a portion of the
subtitle, not all of it, though the rest of the subtitle will usually be shown
in a subsequent display update.

### Window Definition Segment

Window definition segments define where the sub picture will be shown.

The first byte is used as an ID of the window.

The next two bytes are used for the X offset from the left edge of the sceren
for the top left pixel of the window.

The next two bytes are used for the Y offset from the top edge of the screen
for the top left pixel of the window.

The next two bytes are for the width of the window, in pixels.

The next two bytes are for the height of the window, in pixels.

There [appears to be some confusion here][wds-confusion], and it's possible
that there's also a byte used for the number of windows defined in this WDS,
and that the window information repeats that number of times? The patent is
unclear.

### Palette Definition Segment

Palette definition segments define the palette to use when drawing images.

The first byte is used as an ID for the palette.

The next byte is used as the version number of the palette within this epoch.

The next byte is the entry number of the palette.

The next byte is the luminance or Y value.

The next byte is the color difference red or Cr value.

The next byte is the color difference blue or Cb value.

The next byte is the transparency or A value.

The last five bytes of this segment can be repeated, each denoting a new color
in the palette.

### Object Definition Segment

The object definition segment defines the graphics object to be displayed.
These are images, usually with rendered text on a transparent background.

The first two bytes are the object ID.

The next byte is the version of the object.

The next byte is a flag for whether this object is the last in a sequence or
not. If an image is split into a set of consecutive fragments, the last
fragment will have this flag set. There are three possible values for this
byte:

* `0x40`: Last in sequence
* `0x80`: First in sequence
* `0xC0`: First and last in sequence. (`0xC0` == `0x40` | `0x80`)

The next three bytes are the length of the object data, as an integer.

The next two bytes are the width of the image, in pixels.

The next two bytes are the height of the image, in pixels.

The next N bytes (where N is the number specified as the length of the object
data) is a Run-length encoded version of the image.

#### Run-length encoding

Run-length encoding is an image encoding technique that revolves around
efficiently encoding long "runs" (repetitions) of a single color.

* A normal byte represents a single pixel of a color, with the color specified
  by the value of the byte, which should be matched to a color in the palette
  by using the byte's value as the palette entry's ID.
* If a byte has a value of 0, and the next byte's big-endian first two bits are
  set to 0, the remainder of the second byte's bits will specify a number
  between 1 and 63. That number of pixels should be written in color 0.
  Example: `00000000 00LLLLLL` would denote L pixels in color 0.
* If a byte has a value of 0, and the next byte's big-endian first bit is a 0,
  followed by a 1, the remainder of the second byte's bits will specify a
  number between 64 and 16,383. That number of pixels should be written in
  color 0. Example: `00000000 01LLLLLL` would denote L pixels in color 0, where
  L is between 64 and 16,383.
* If a byte has a value of 0, and the next byte's big-endian first bit is a 1,
  followed by a 0, the remainder of the second byte's bits will specify a
  number between 1 and 63. That number of pixels should be written in the color
  with an ID matching the third byte. For example, `00000000 10LLLLLL CCCCCCCC`
  would denote L pixels (between 3 and 63) in the color designated by CCCCCCCC.
    * This method will only be used when at least three pixels in the same
      color are being used in a row, because otherwise it's cheaper to specify
      the pixels as one-offs.
* If a byte has a value of 0, and the next byte's big-endian first bit is a 1,
  followed by a 1, the remainder of the second byte's bits and the third byte 
  will specify a number between 64 and 16,383. That number of pixels should be
  written in the color with an ID matching the fourth byte. For example,
  `00000000 11LLLLLL LLLLLLLL CCCCCCCC` would be L pixels (between 64 and
  16,383) in the color designated by CCCCCCCC.
* If two 0 bytes are seen, that designates the end of a line, and future pixels
  should be written starting from the left side of the image, one pixel lower
  than the current line.

### End of Display Set Segment

The end segment always has a segment size of zero, and just serves as a marker
for the end of a display set.

## Display Sets

A display set consists of a Presentation Composition Segment, followed by one
or more Window Definition Segments, followed by one or more Palette Definition
Segments, followed by one or more Object Definition Segments, followed by an
End of Display Set Segment.

It's unclear if the ordering of the Window, Palette, and Object Definition
Segments within a Display Set are guaranteed.

## Inside MKV Files

PGS segments are stored in MKV blocks, with the segment's presentation
timestamp matching the block's timestamp.

Because PGS segments are to be shown until a new PGS segment is encountered, a
MKV block _may_ have no duration. In this case, the subtitle should continue to
be shown until the next PGS segment is encountered. A blank PGS segment may be
used to clear subtitles. A muxer may use a duration on the the MKV block and
take on the calculation of the time between PGS segments itself. If a block has
a duration, the PGS segment must only be displayed for the duration specified
in the block.

## Sources

* [Matroska Subtitles][matroska-subs]
* [Presentation Graphic Stream (SUP files) BluRay Subtitle Format on
  blog.thescorpius.com][scorpius]

[matroska-subs]: http://matroska-org.github.io/matroska-specification/subtitles.html "Matroska
Subtitles"
[scorpius]: http://blog.thescorpius.com/index.php/2017/07/15/presentation-graphic-stream-sup-files-bluray-subtitle-format/ "PGS format blog post"
[wds-confusion]: http://blog.thescorpius.com/index.php/2017/07/15/presentation-graphic-stream-sup-files-bluray-subtitle-format/#comment-158 "Comment about WDS lack of clarity"
