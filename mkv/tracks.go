package mkv

func TrackNumberSize(b []byte) int {
	n, v, bit := 1, b[0], uint8(0x80)
	for bit > 0 && v&bit == 0 {
		n++
		bit >>= 1
	}
	return n
}

func ParseTrackNumber(b []byte) int64 {
	n, v, bit := 1, b[0], uint8(0x80)
	for bit > 0 && v&bit == 0 {
		n++
		bit >>= 1
	}
	bit--
	i := int64(v & bit)
	if n > 1 {
		for _, v := range b[1:n] {
			i = (i << 8) | int64(v)
		}
	}
	return i
}
