package mkv

type Block struct {
	Offset          int64
	Size            int64
	Level           int
	Raw             []byte
	TrackNo         int64
	Timecode        int16
	ClusterTimecode int64
	Flags           FlagSet
}
