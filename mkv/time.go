package mkv

import (
	"bytes"
	"encoding/binary"
)

func ParseTimecode(b []byte) (int16, error) {
	offset := TrackNumberSize(b)
	timecodeRaw := bytes.NewBuffer(b[offset : offset+2])
	var timecode int16
	err := binary.Read(timecodeRaw, binary.BigEndian, &timecode)
	if err != nil {
		return 0, err
	}
	return timecode, nil
}
