package mkv

type FlagSet struct {
	KeyFrame    bool
	Invisible   bool
	NoLacing    bool
	XiphLacing  bool
	EbmlLacing  bool
	FixedLacing bool
	Discardable bool
}

func ParseFlags(b []byte) FlagSet {
	offset := TrackNumberSize(b) + 2 // two for timecode
	flagByte := b[offset]
	return FlagSet{
		KeyFrame:    flagByte&128 == 1,
		Invisible:   flagByte&8 == 1,
		NoLacing:    flagByte&4 == 0 && flagByte&2 == 0,
		XiphLacing:  flagByte&4 == 0 && flagByte&2 == 1,
		EbmlLacing:  flagByte&4 == 1 && flagByte&2 == 1,
		FixedLacing: flagByte&4 == 1 && flagByte&2 == 1,
		Discardable: flagByte&1 == 1,
	}
}
