package main

import (
	"bytes"
	"crypto/sha256"
	"encoding/binary"
	"encoding/hex"
	"flag"
	"fmt"
	"image/png"
	"io/ioutil"
	"os"
	"path/filepath"
	"strings"
	"time"

	"code.carvers.co/subtitler/mkv"
	"code.carvers.co/subtitler/pgs"
	"github.com/otiai10/gosseract"
	mkvparse "github.com/remko/go-mkvparse"
	yall "yall.in"
	"yall.in/colour"
)

type SubtitleParser struct {
	blocks          []mkv.Block
	timecodeScale   int64
	clusterTimecode int64
	subtitleTracks  map[int64]struct{}
	curTrackNumber  int64
	inTrackEntry    bool
	language        string
	log             *yall.Logger
	debug           bool
}

func (p *SubtitleParser) HandleMasterBegin(id mkvparse.ElementID, info mkvparse.ElementInfo) (bool, error) {
	if id == mkvparse.TrackEntryElement {
		p.inTrackEntry = true
	}
	return true, nil
}

func (p *SubtitleParser) HandleMasterEnd(id mkvparse.ElementID, info mkvparse.ElementInfo) error {
	if id == mkvparse.TrackEntryElement {
		p.inTrackEntry = false
	}
	return nil
}

func (p *SubtitleParser) HandleString(id mkvparse.ElementID, value string, info mkvparse.ElementInfo) error {
	if id == mkvparse.LanguageElement && p.inTrackEntry {
		p.language = strings.ToLower(value)
	}
	return nil
}

func (p *SubtitleParser) HandleInteger(id mkvparse.ElementID, value int64, info mkvparse.ElementInfo) error {
	if id == mkvparse.TimecodeElement {
		p.clusterTimecode = value
	}
	if id == mkvparse.TimecodeScaleElement {
		p.timecodeScale = value
	}
	if id == mkvparse.TrackNumberElement && p.inTrackEntry {
		p.curTrackNumber = value
	}
	if id == mkvparse.TrackTypeElement && p.inTrackEntry && value == 0x11 && (p.language == "eng" || p.language == "eng-us") {
		p.subtitleTracks[p.curTrackNumber] = struct{}{}
	}
	return nil
}

func (p *SubtitleParser) HandleFloat(id mkvparse.ElementID, value float64, info mkvparse.ElementInfo) error {
	return nil
}

func (p *SubtitleParser) HandleDate(id mkvparse.ElementID, value time.Time, info mkvparse.ElementInfo) error {
	return nil
}

func (p *SubtitleParser) HandleBinary(id mkvparse.ElementID, value []byte, info mkvparse.ElementInfo) error {
	if id == mkvparse.SimpleBlockElement || id == mkvparse.BlockElement {
		b := mkv.Block{
			Offset:          info.Offset,
			Size:            info.Size,
			Level:           info.Level,
			Raw:             value,
			TrackNo:         mkv.ParseTrackNumber(value),
			ClusterTimecode: p.clusterTimecode,
			Flags:           mkv.ParseFlags(value),
		}
		if _, ok := p.subtitleTracks[b.TrackNo]; !ok {
			return nil
		}
		timecode, err := mkv.ParseTimecode(value)
		if err != nil {
			return err
		}
		b.Timecode = timecode
		p.blocks = append(p.blocks, b)
	}
	return nil
}

func (p *SubtitleParser) blockToSUP(b mkv.Block) ([]pgs.Segment, error) {
	log := p.log.WithField("track", b.TrackNo)
	var segments []pgs.Segment
	offset := mkv.TrackNumberSize(b.Raw) + 2 + 1 // two for timecode, one for flag
	log.WithField("timecode", b.Raw[mkv.TrackNumberSize(b.Raw):mkv.TrackNumberSize(b.Raw)+2]).Debug("skipped timecode")
	log.WithField("flag", b.Raw[mkv.TrackNumberSize(b.Raw)+2:mkv.TrackNumberSize(b.Raw)+2+1]).Debug("skipped flag")
	for offset < len(b.Raw) {
		sup := b.Raw[offset:]
		size := binary.BigEndian.Uint16(sup[1:3]) // first byte is type, second and third are size
		seg := pgs.Segment{
			PresentationTimestamp: (int64(b.Timecode) * p.timecodeScale) + (p.timecodeScale * b.ClusterTimecode),
			SegmentType:           sup[0],
			Bytes:                 sup[3 : size+3], // three for size and type
		}
		if len(seg.Bytes) != int(size) {
			return nil, fmt.Errorf("Expected %d bytes in segment, got %d", size, len(seg.Bytes))
		}
		segments = append(segments, seg)
		offset += int(size + 3)
	}

	last := segments[len(segments)-1]
	last.LastInBlock = true
	segments[len(segments)-1] = last

	return segments, nil
}

func main() {
	var pathToMKV, textOutputDir, imgOutputDir string
	flag.StringVar(&textOutputDir, "text-dir", "", "directory to output OCR text for subtitles")
	flag.StringVar(&imgOutputDir, "image-dir", "", "directory to output subtitle images to")
	flag.StringVar(&pathToMKV, "mkv", "", "path to the file to pull subtitles from")
	flag.Parse()

	if textOutputDir == "" || imgOutputDir == "" || pathToMKV == "" {
		flag.Usage()
		os.Exit(1)
	}

	err := os.MkdirAll(textOutputDir, 0777)
	if err != nil {
		fmt.Printf("Error creating directory %q: %s\n", textOutputDir, err.Error())
		os.Exit(1)
	}
	err = os.MkdirAll(imgOutputDir, 0777)
	if err != nil {
		fmt.Printf("Error creating directory %q: %s\n", imgOutputDir, err.Error())
		os.Exit(1)
	}

	handler := SubtitleParser{subtitleTracks: map[int64]struct{}{}, log: yall.New(colour.New(os.Stdout, yall.Debug))}
	err = mkvparse.ParsePath(pathToMKV, &handler)
	if err != nil {
		fmt.Printf("Error parsing %q: %s\n", pathToMKV, err.Error())
		os.Exit(1)
	}

	var segs []pgs.Segment
	for pos, block := range handler.blocks {
		segments, err := handler.blockToSUP(block)
		if err != nil {
			fmt.Printf("Error turning block %d of %q into SUP file: %s\n", pos, pathToMKV, err.Error())
			os.Exit(1)
		}
		segs = append(segs, segments...)
	}
	stream, err := pgs.ParseStream(segs)
	if err != nil {
		fmt.Printf("Error parsing PGS stream from %q: %s\n", pathToMKV, err.Error())
		os.Exit(1)
	}

	subs, err := pgs.SubtitlesFromStream(stream)
	if err != nil {
		fmt.Printf("Error generating images from %q: %s\n", pathToMKV, err.Error())
		os.Exit(1)
	}

	results := map[string][]pgs.Subtitle{}
	for pos, sub := range subs {
		var b bytes.Buffer
		err = png.Encode(&b, sub.Image)
		if err != nil {
			fmt.Printf("Error encoding image %d to PNG: %s\n", pos, err.Error())
			os.Exit(1)
		}
		shaTmp := sha256.Sum256(b.Bytes())
		sha := hex.EncodeToString(shaTmp[:])
		path := filepath.Join(imgOutputDir, sha+".png")
		if _, err := os.Stat(path); err == nil {
			// equivalent file already exists, ignore it
			continue
		} else if !os.IsNotExist(err) {
			fmt.Printf("Error checking if %q exists: %s\n", path, err.Error())
			continue
		}
		err = ioutil.WriteFile(path, b.Bytes(), 0777)
		if err != nil {
			fmt.Printf("Error writing %q: %s\n", path, err.Error())
			continue
		}
		results[sha] = append(results[sha], sub)
	}
	for sha := range results {
		imagePath := filepath.Join(imgOutputDir, sha+".png")
		textPath := filepath.Join(textOutputDir, sha+".txt")
		text, err := ocr(imagePath)
		if err != nil {
			fmt.Printf("Error running OCR on %q: %s\n", imagePath, err.Error())
			continue
		}
		text = strings.TrimSpace(text)
		err = ioutil.WriteFile(textPath, []byte(text), 0777)
		if err != nil {
			fmt.Printf("Error writing OCR results to %q: %s\n", textPath, err.Error())
			continue
		}
	}
}

func ocr(imagePath string) (string, error) {
	tessClient := gosseract.NewClient()
	defer tessClient.Close()

	tessClient.SetImage(imagePath)
	return tessClient.Text()
}
